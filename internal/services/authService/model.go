package authservice

import (
	"context"

	"github.com/jackc/pgx/v5"
)

type profile struct {
	Id       string `db:"id"`
	Email    string `db:"email"`
	Password string `db:"pass"`
}

func (service *AuthService) CreateAccount(email string, pass string) error {
	query := "INSERT INTO profile (email, pass) VALUES (@email, @pass)"
	args := pgx.NamedArgs{
		"email": email,
		"pass":  pass,
	}
	_, err := service.Db.ConnectionPool.Exec(context.Background(), query, args)
	if err != nil {
		return err
	}
	return nil
}

func (service *AuthService) GetAccount(email string, pass string) (profile, error) {
	data := profile{}
	query := "SELECT * FROM profile WHERE email = @email and pass = @pass"
	args := pgx.NamedArgs{
		"email": email,
		"pass":  pass,
	}
	row, err := service.Db.ConnectionPool.Query(context.Background(), query, args)

	if err != nil {
		return data, err
	}

	data, err = pgx.CollectOneRow(pgx.Rows(row), pgx.RowToStructByName[profile])

	if err != nil {
		return data, err
	}
	return data, nil

}

func (service *AuthService) GetAccountId(email string) (profile,error){
	data := profile{}
	query := "SELECT * FROM profile WHERE email = @email"
	args := pgx.NamedArgs{
		"email": email,
	}
	row, err := service.Db.ConnectionPool.Query(context.Background(), query, args)

	if err != nil {
		return data, err
	}

	data, err = pgx.CollectOneRow(pgx.Rows(row), pgx.RowToStructByName[profile])

	if err != nil {
		return data, err
	}
	return data, nil
}
