package authservice

import (
	"errors"
	"fmt"
	libs "goHtmx/internal/core"
	views "goHtmx/web/templates"
	"net/http"
	"strconv"

	"github.com/jackc/pgx/v5"
	"github.com/labstack/echo/v4"
)

type AuthService struct {
	Db libs.CockroachDb
}

var logger = libs.GetLogger()

func (service *AuthService) CreateAccountHandler(c echo.Context) error {
	email := c.FormValue("email")
	pass := c.FormValue("password")

	if email == "" || pass == "" {
		return echo.NewHTTPError(http.StatusInternalServerError, "BAD PARAMS")
	}
	err := service.CreateAccount(email, pass)
	if err != nil {
		pgErr := libs.GetSqlError(err)
		if pgErr.Code == strconv.Itoa(libs.SqlDuplicateKey) {
			return libs.Render(c, http.StatusOK, views.Index(views.Register(true)))
		} else {
			libs.HandleSimpleError(err, "AuthService::CreateAccountHandler")
			return echo.NewHTTPError(http.StatusInternalServerError, "INTERNAL SERVER ERROR")
		}
	}
	return libs.Render(c, http.StatusOK, views.Index(views.Form(false)))
}

func (service *AuthService) RegisterHandler(c echo.Context) error {
	return libs.Render(c, http.StatusOK, views.Index(views.Register(false)))
}

func (service *AuthService) LoginHandler(c echo.Context) error {
	email := c.FormValue("email")
	pass := c.FormValue("password")
	if email == "" || pass == "" {
		return echo.NewHTTPError(http.StatusInternalServerError, "BAD PARAMS")
	}

	data, err := service.GetAccount(email, pass)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return libs.Render(c, http.StatusOK, views.Index(views.Form(true)))
		} else {
			libs.HandleSimpleError(err, "AuthService::LoginHandler")
			return echo.NewHTTPError(http.StatusInternalServerError, "BAD PARAMS")
		}
	}
	fmt.Printf("%#v",data)
	cookie := libs.GenerateCookie(email, data.Id)
	c.SetCookie(cookie)
	c.Response().Header().Set("HX-Location","/dashboard")
	return c.NoContent(http.StatusOK)
}
