package authservice

import (
	"fmt"
	libs "goHtmx/internal/core"
	views "goHtmx/web/templates"
	"net/http"

	"github.com/labstack/echo/v4"
)

type TaskService struct {
	Db libs.CockroachDb
}

var logger = libs.GetLogger()

func (service *TaskService) RootHandler(c echo.Context) error {
	return libs.Render(c, http.StatusOK, views.Index(views.Form(false)))
}
func (service *TaskService) DashboardHandler(c echo.Context) error {
	cookie, _ := c.Cookie("token")
	claims := libs.GetClaims(*cookie)
	fmt.Printf("%#v",claims.Id)
	return libs.Render(c, http.StatusOK, views.Index(views.Content()))
}
