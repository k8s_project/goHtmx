package libs

import (
	"github.com/a-h/templ"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/labstack/echo/v4"
)
var SqlDuplicateKey = 23505

func Render(ctx echo.Context, statusCode int, t templ.Component) error {
	ctx.Response().Writer.WriteHeader(statusCode)
	ctx.Response().Header().Set(echo.HeaderContentType, echo.MIMETextHTML)
	return t.Render(ctx.Request().Context(), ctx.Response().Writer)
}

func GetSqlError(err error) *pgconn.PgError{
	var pgErr *pgconn.PgError = err.(*pgconn.PgError)
	return pgErr
}