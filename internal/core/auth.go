package libs

import (
	"net/http"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/spf13/viper"
)

type JwtCustomClaims struct {
	Id    string `json:"id"`
	Email  string `json:"name"`
	Admin bool   `json:"admin"`
	jwt.RegisteredClaims
}

func GenerateCookie(email string, id string) *http.Cookie {
	claims := &JwtCustomClaims{
		id,
		email,
		true,
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * 72)),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte(viper.GetString("JWT_SECRET")))
	if err != nil {
		// return err
	}
	cookie := &http.Cookie{
		Name:     "token",
		Value:    t,
		Expires:  time.Now().Add(time.Hour * 1),
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
		Secure:   false,
	}
	return cookie
}

func GetClaims(cookie http.Cookie) *JwtCustomClaims{
	extractedData := cookie.Value
	token, _ := jwt.ParseWithClaims(extractedData, &JwtCustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(viper.GetString("JWT_SECRET")), nil
	})
	claims := token.Claims.(*JwtCustomClaims)
	return claims
}
