package api

import (
	customMiddlewares "goHtmx/api/middlewares"
	libs "goHtmx/internal/core"
	authservice "goHtmx/internal/services/authService"
	taskservice "goHtmx/internal/services/tasksService"

	"github.com/golang-jwt/jwt/v5"
	echojwt "github.com/labstack/echo-jwt/v4"
	"github.com/labstack/echo/v4"
	"github.com/spf13/viper"
)

func InitHttpServer() *echo.Echo {
	logger := libs.GetLogger()
	//-------------DB AND SERVICES---------
	db := libs.CockroachDb{}
	db.CreateCockRoachConnection()
	defer db.CloseConnection()

	authserviceInstance := authservice.AuthService{Db: db}
	taskserviceInstance := taskservice.TaskService{}

	e := echo.New()
	//-------------MIDDLEWARES-------------
	customMiddlewares.LoggerMiddleware(e, logger)
	//---------------ROUTES----------------
	e.GET("/", func(c echo.Context) error {
		return taskserviceInstance.RootHandler(c)
	})
	e.GET("/register", func(c echo.Context) error {
		return authserviceInstance.RegisterHandler(c)
	})
	e.POST("/login", func(c echo.Context) error {
		return authserviceInstance.LoginHandler(c)
	})
	e.POST("/createAccount", func(c echo.Context) error {
		return authserviceInstance.CreateAccountHandler(c)
	})
	e.Static("/static/*", "web/static")
	users := e.Group("/")
	{
		config := echojwt.Config{
			NewClaimsFunc: func(c echo.Context) jwt.Claims {
				return new(libs.JwtCustomClaims)
			},
			SigningKey:  []byte(viper.GetString("JWT_SECRET")),
			TokenLookup: "cookie:token",
		}
		users.Use(echojwt.WithConfig(config))
		users.GET("dashboard", func(c echo.Context) error {
			return taskserviceInstance.DashboardHandler(c)
		})
	}

	//-------------------------------------
	logger.Info().Msg("Starting server on port 3000")
	e.Start(":3000")

	return e
}


